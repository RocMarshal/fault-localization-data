#!/usr/bin/env bash
#
# ------------------------------------------------------------------------------
# This script submits as many jobs (if executed on a cluster) or background
# processes as the number of projects * number of bugs. Each job scores a
# specified D4J project/bug.
# 
# Usage:
# ./jobs.sh
#   --output_dir <path>
#   [--help]
# 
# Environment variables:
# - D4J_HOME             Needs to be set and must point to the Defects4J installation.
# - GZOLTAR_FILES_DIR    Needs to be set and must point to GZoltar's tar.gz files.
# - KILLMAPS_FILES_DIR   Needs to be set and must point to KillMap tar.gz files.
# 
# ------------------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../gzoltar/utils.sh" || exit 1

# ------------------------------------------------------------------ Envs & Args

# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "D4J_HOME is not set!"
[ -d "$D4J_HOME" ] || die "$D4J_HOME does not exist!"
export DEFECTS4J_HOME="$D4J_HOME"

# Check whether GZOLTAR_FILES_DIR is set
[ "$GZOLTAR_FILES_DIR" != "" ] || die "GZOLTAR_FILES_DIR is not set!"
[ -d "$GZOLTAR_FILES_DIR" ] || die "$GZOLTAR_FILES_DIR does not exist!"

# Check whether KILLMAPS_FILES_DIR is set
[ "$KILLMAPS_FILES_DIR" != "" ] || die "KILLMAPS_FILES_DIR is not set!"
[ -d "$KILLMAPS_FILES_DIR" ] || die "$KILLMAPS_FILES_DIR does not exist!"

# Check whether BLACKLIST_FILE exists
BLACKLIST_FILE="$SCRIPT_DIR/../data/blacklist.csv"
[ -s "$BLACKLIST_FILE" ] || die "$BLACKLIST_FILE file does not exist or it is empty!"

USAGE="Usage: ${BASH_SOURCE[0]} --output_dir <path> [help]"
if [ "$#" -ne "1" ] && [ "$#" -ne "2" ]; then
  die "$USAGE"
fi

OUTPUT_DIR=""

while [[ "$1" = --* ]]; do
  OPTION=$1; shift
  case $OPTION in
    (--output_dir)
      OUTPUT_DIR=$1;
      shift;;
    (--help)
      echo "$USAGE"
      exit 0;;
    (*)
      die "$USAGE";;
  esac
done

[ "$OUTPUT_DIR" != "" ] || die "$USAGE"
[ -d "$OUTPUT_DIR" ] || mkdir -p "$OUTPUT_DIR"

# ------------------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do

  for bid in $(cut -f1 -d',' "$D4J_HOME/framework/projects/$pid/commit-db"); do

    if grep -q "^$pid,.*,$bid," "$BLACKLIST_FILE"; then
      continue
    fi

    if [ "$bid" -gt "1000" ]; then
      MUTANTS_IN_SCOPE="$D4J_HOME/framework/projects/$pid/mutants_in_scope.csv"
      if ! grep -q "^$pid,.*,$bid$" "$MUTANTS_IN_SCOPE"; then
        continue # not in scope
      fi
    fi

    scoring_dir="$OUTPUT_DIR/$pid/$bid"
    log_file="$scoring_dir/log.txt"
    if [ -f "$log_file" ]; then
      if tail -n1 "$log_file" | grep -q "^job has finished successfully\!$" "$log_file" && \
         ! grep -q " No space left on device$" "$log_file" && \
         ! grep -q "\[ERROR\] " "$log_file"; then
        if [ -s "$scoring_dir/scores.tar.gz" ]; then
          continue
        fi
      fi
    fi

    rm -rf "$scoring_dir"
    mkdir -p "$scoring_dir"

    echo "$pid-$bid"
    echo "$pid-$bid" > "$log_file"

    pushd . > /dev/null 2>&1
    cd "$SCRIPT_DIR"
      if _am_I_a_cluster; then
        qsub -V -N "_$bid-$pid" -l h_rt=08:00:00 -l rmem=4G -e "$log_file" -o "$log_file" -j y \
          "job.sh" --project "$pid" --bug "$bid" --output_dir "$scoring_dir"
      else
        timeout --signal=KILL "8h" bash \
          "job.sh" --project "$pid" --bug "$bid" --output_dir "$scoring_dir" > "$log_file" 2>&1 &
        _register_background_process $!
        _can_more_jobs_be_submitted
      fi
    popd > /dev/null 2>&1
  done
done

# It might be the case that there are still jobs running
_wait_for_jobs;

echo "All jobs have been submitted!"
exit 0
