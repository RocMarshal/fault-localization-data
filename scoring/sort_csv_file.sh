#!/usr/bin/env bash
#
# --------------------------------------------------------------------
# This script sorts a .csv file by project and then by bug id, and
# stores the result in a file called "-sorted".
#
# Usage:
# bash sort_csv_file.sh <path to csv file>
#
# Environment variables:
# - D4J_HOME      Needs to be set and must point to the Defects4J
#                 installation.
#
# --------------------------------------------------------------------

HERE=$(cd `dirname $0` && pwd)

#
# Print error message and exit
#
die() {
  echo $1
  exit 1
}

# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "D4J_HOME is not set!"
[ -d "$D4J_HOME" ] || die "$D4J_HOME does not exist!"

# Check whether BLACKLIST_FILE is set
export BLACKLIST_FILE="$HERE/../data/blacklist.csv"
[ "$BLACKLIST_FILE" != "" ] || die "BLACKLIST_FILE is not set!"
[ -s "$BLACKLIST_FILE" ] || die "$BLACKLIST_FILE file does not exist or it is empty!"

USAGE="Usage: ${BASH_SOURCE[0]} <path to csv file>"
[ $# -eq 1 ] || die "$USAGE"

INPUT_FILE="$1"
if [ ! -s "$INPUT_FILE" ]; then
  die "'$INPUT_FILE' is empty or does not exist!"
fi
OUTPUT_FILE="$INPUT_FILE-sorted"

# get header
head -n1 "$INPUT_FILE" > "$OUTPUT_FILE"

# sort content
for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"

  # Get list of real and artificial faults
  bids=$(cut -f1 -d',' $dir_project/commit-db)

  # Iterate over all bugs (real and artificial) for this project
  for bid in $bids; do
    if grep -q "^$pid,.*,$bid," "$BLACKLIST_FILE"; then
      continue
    fi

    if ! grep -q "^$pid,$bid," "$INPUT_FILE"; then
      # not all csv files have data for all bugs
      continue
    fi

    echo "$pid,$bid"
    grep "^$pid,$bid," "$INPUT_FILE" | sort >> "$OUTPUT_FILE"
  done
done

# sanity check
num_lines_original=$(wc -l "$INPUT_FILE" | cut -f1 -d' ')
num_lines_sorted=$(wc -l "$OUTPUT_FILE" | cut -f1 -d' ')

if [ "$num_lines_original" -ne "$num_lines_sorted" ]; then
  die "Expecting '$num_lines_original' lines but sorted list only has '$num_lines_sorted'"
fi

echo "DONE!"
exit 0
