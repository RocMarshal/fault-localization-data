#!/usr/bin/env bash
# ------------------------------------------------------------------------------
# 
# This script scores a specified D4J project/bug
# 
# Usage:
# ./job.sh
#   --project <project_name>
#   --bug <bug_id>
#   --output_dir <path>
#   [--help]
# 
# Environment variables:
# - D4J_HOME             Needs to be set and must point to the Defects4J installation.
# - GZOLTAR_FILES_DIR    Needs to be set and must point to GZoltar's tar.gz files.
# - KILLMAPS_FILES_DIR   Needs to be set and must point to KillMap tar.gz files.
# 
# ------------------------------------------------------------------------------

SCRIPT_DIR=`pwd`
source "$SCRIPT_DIR/../gzoltar/utils.sh" || exit 1

# ------------------------------------------------------------------ Envs & Args

# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "D4J_HOME is not set!"
[ -d "$D4J_HOME" ] || die "$D4J_HOME does not exist!"
export DEFECTS4J_HOME="$D4J_HOME"

# Check whether GZOLTAR_FILES_DIR is set
[ "$GZOLTAR_FILES_DIR" != "" ] || die "GZOLTAR_FILES_DIR is not set!"
[ -d "$GZOLTAR_FILES_DIR" ] || die "$GZOLTAR_FILES_DIR does not exist!"

# Check whether KILLMAPS_FILES_DIR is set
[ "$KILLMAPS_FILES_DIR" != "" ] || die "KILLMAPS_FILES_DIR is not set!"
[ -d "$KILLMAPS_FILES_DIR" ] || die "$KILLMAPS_FILES_DIR does not exist!"

USAGE="Usage: ${BASH_SOURCE[0]} --project <project_name> --bug <bug_id> --output_dir <path> [help]"
if [ "$#" -ne "1" ] && [ "$#" -ne "6" ]; then
  die "$USAGE"
fi

PID=""
BID=""
OUTPUT_DIR=""

while [[ "$1" = --* ]]; do
  OPTION=$1; shift
  case $OPTION in
    (--project)
      PID=$1;
      shift;;
    (--bug)
      BID=$1;
      shift;;
    (--output_dir)
      OUTPUT_DIR=$1;
      shift;;
    (--help)
      echo "$USAGE"
      exit 0;;
    (*)
      die "$USAGE";;
  esac
done

[ "$PID" != "" ] || die "$USAGE"
[ "$BID" != "" ] || die "$USAGE"

[ "$OUTPUT_DIR" != "" ] || die "$USAGE"
[ -d "$OUTPUT_DIR" ] || mkdir -p "$OUTPUT_DIR"

SCORES_FILES="$OUTPUT_DIR/scores.csv"
>"$SCORES_FILES" || die "Cannot write to $SCORES_FILES!"

# ------------------------------------------------------------------------- Main

echo "PID: $$"
hostname
python --version

TMP_DATA_DIR="/tmp/$USER-$PID-$BID-$$"
rm -rf "$TMP_DATA_DIR"
mkdir -p "$TMP_DATA_DIR" || die "[ERROR] Cannot create temporary directory '$TMP_DATA_DIR'!"

# Unpack Killmap files

KILLMAP_FILE="$KILLMAPS_FILES_DIR/$PID/$BID/killmap-files.tar.gz"
if [ ! -s "$KILLMAP_FILE" ]; then
  KILLMAP_FILE="$KILLMAPS_FILES_DIR/$PID/$BID/168h-killmap-files.tar.gz"
  if [ ! -s "$KILLMAP_FILE" ]; then
    KILLMAP_FILE="$KILLMAPS_FILES_DIR/$PID/$BID/32h-killmap-files.tar.gz"
    if [ ! -s "$KILLMAP_FILE" ]; then
      rm -rf "$TMP_DATA_DIR"
      die "[ERROR] killmap-files.tar.gz does not exist or it is empty!"
    fi
  fi
fi

tar xzf "$KILLMAP_FILE" -C "$TMP_DATA_DIR"
if [ $? -ne 0 ]; then
  rm -rf "$TMP_DATA_DIR"
  die "[ERROR] Extraction of '$KILLMAP_FILE' has failed!"
fi

KILLMAP_FILE="$TMP_DATA_DIR/killmaps/$PID/$BID/killmap.csv.gz"
if [ ! -s "$KILLMAP_FILE" ]; then
  rm -rf "$TMP_DATA_DIR"
  die "[ERROR] $KILLMAP_FILE does not exist or it is empty!"
fi
MUTANTS_LOG="$TMP_DATA_DIR/killmaps/$PID/$BID/mutants.log"
if [ ! -s "$MUTANTS_LOG" ]; then
  rm -rf "$TMP_DATA_DIR"
  die "[ERROR] $MUTANTS_LOG does not exist or it is empty!"
fi

# Unpack GZoltar files

GZOLTAR_FILE="$GZOLTAR_FILES_DIR/$PID/$BID/gzoltar-files.tar.gz"
if [ ! -s "$GZOLTAR_FILE" ]; then
  rm -rf "$TMP_DATA_DIR"
  die "[ERROR] $GZOLTAR_FILE does not exist or it is empty!"
fi

tar xzf "$GZOLTAR_FILE" -C "$TMP_DATA_DIR"
if [ $? -ne 0 ]; then
  rm -rf "$TMP_DATA_DIR"
  die "[ERROR] Extraction of '$GZOLTAR_FILE' has failed!"
fi

MATRIX_FILE="$TMP_DATA_DIR/gzoltars/$PID/$BID/matrix"
if [ ! -s "$MATRIX_FILE" ]; then
  rm -rf "$TMP_DATA_DIR"
  die "[ERROR] $MATRIX_FILE does not exist or it is empty!"
fi
SPECTRA_FILE="$TMP_DATA_DIR/gzoltars/$PID/$BID/spectra"
if [ ! -s "$SPECTRA_FILE" ]; then
  rm -rf "$TMP_DATA_DIR"
  die "[ERROR] $SPECTRA_FILE does not exist or it is empty!"
fi

#
# Score
#

for scoring_script in "do-previously-studied-flts" "do-full-analysis"; do
  if [ "$scoring_script" == "do-full-analysis" ]; then
    if [ "$BID" -gt "1000" ]; then
      # `do-full-analysis` is only performed on real faults
      break
    fi
  fi

  echo "Scoring: $scoring_script"
  scoring_dir="$OUTPUT_DIR/$scoring_script"
  scoring_scores_file="$scoring_dir/scores.csv"

  pushd . > /dev/null 2>&1
  cd "$SCRIPT_DIR/../analysis/pipeline-scripts/"
    bash "$scoring_script" \
      "$PID" "$BID" "developer" "$MATRIX_FILE" "$SPECTRA_FILE" "$KILLMAP_FILE" "$MUTANTS_LOG" \
      "$scoring_dir" "$scoring_scores_file"
    if [ $? -ne 0 ]; then
      rm -rf "$TMP_DATA_DIR"
      die "[ERROR] Scoring of '$PID-$BID' has failed!"
    fi
  popd > /dev/null 2>&1
  [ -s "$scoring_scores_file" ] || die "$scoring_scores_file does not exist or it is empty!"

  # Sanity check scores file
  num_entries=$(wc -l "$scoring_scores_file" | cut -f1 -d' ')
  [ "$num_entries" -ge "2" ] || die "File $scoring_scores_file is incomplete!"

  # Collect scores in a single file
  if [ -s "$SCORES_FILES" ]; then
    tail -n +2 "$scoring_scores_file" >> "$SCORES_FILES"
  else
    cat "$scoring_scores_file" > "$SCORES_FILES"
  fi
done
[ -s "$SCORES_FILES" ] || die "$SCORES_FILES does not exist or it is empty!"

rm -rf "$TMP_DATA_DIR" # Clean up

if grep -q ",$" "$SCORES_FILES"; then
  die "$SCORES_FILES is not well formatted!"
fi

#
# Zip data
#

echo "job has finished successfully!"

pushd . > /dev/null 2>&1
cd "$OUTPUT_DIR"

  if [ "$BID" -lt "1000" ]; then
    tar -czf "scores.tar.gz" \
      "do-previously-studied-flts/" \
      "do-full-analysis/" \
      "log.txt" \
      "scores.csv" || die "[ERROR] It was not possible to compress directory '$OUTPUT_DIR/'!"
  else
    tar -czf "scores.tar.gz" \
      "do-previously-studied-flts/" \
      "log.txt" \
      "scores.csv" || die "[ERROR] It was not possible to compress directory '$OUTPUT_DIR/'!"
  fi

popd > /dev/null 2>&1

exit 0
