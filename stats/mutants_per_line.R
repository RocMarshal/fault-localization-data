#
# This script computes the ratio of mutants used in the MBFL experiments.
#
# Usage:
#   Rscript mutants_per_line.R <mutants_per_line_data_file> <executable_lines_data_file>
#

args <- commandArgs(trailingOnly = TRUE)
if (length(args) != 2) {
  stop("Usage: Rscript mutants_per_line.R <mutants_per_line_data_file> <executable_lines_data_file>")
}

sloc_file <- "../analysis/pipeline-scripts/buggy-lines/sloc.csv"
sloc_df <- read.csv(file=sloc_file, header=TRUE) # project_id,bug_id,sloc,sloc_total
sloc_df <- sloc_df[ , c("project_id", "bug_id", "sloc"), drop=FALSE]
# Rename columns for a simpler merge
colnames(sloc_df)[which(names(sloc_df) == "project_id")] <- "pid"
colnames(sloc_df)[which(names(sloc_df) == "bug_id")] <- "bid"

mutants_per_line_file <- args[1]
executable_lines_file <- args[2]

mutants_per_line_df <- read.csv(file=mutants_per_line_file, header=TRUE) # pid,bid,operator,line
executable_lines_df <- read.csv(file=executable_lines_file, header=TRUE) # pid,bid,line

mutants_per_line_and_sloc <- merge(sloc_df, mutants_per_line_df, by=c("pid", "bid"), all.x=TRUE)

#
# Ratio of mutated lines = # unique mutated lines per pid-bid / # lines in loaded classes
#
cat("\n=== Ratio of mutated lines ===\n", sep="")
# Count number of unique mutated lines per pid-bid
mutated_lines <- aggregate(line ~ pid + bid + sloc, data=mutants_per_line_and_sloc, FUN=function(x) length(unique(x)))
colnames(mutated_lines)[which(names(mutated_lines) == "line")] <- "num_unique_mutated_lines"
# Compute ratio per pid-bid
ratio_mutated_lines <- mutated_lines$"num_unique_mutated_lines" / mutated_lines$"sloc"
cat("Min: ", min(ratio_mutated_lines), "\n", sep="")
cat("Max: ", max(ratio_mutated_lines), "\n", sep="")
cat("Mean: ", mean(ratio_mutated_lines), "\n", sep="")
cat("Median: ", median(ratio_mutated_lines), "\n", sep="")

#
# Ratio of mutants per line = # of mutants per pid-bid / # lines in loaded classes
#
cat("\n=== Ratio of mutants per line ===\n", sep="")
# Count number of mutants per pid-bid
mutants_per_line <- aggregate(line ~ pid + bid + sloc, data=mutants_per_line_and_sloc, FUN=length)
colnames(mutants_per_line)[which(names(mutants_per_line) == "line")] <- "num_mutants"
# Compute ratio per pid-bid
ratio_mutants_per_line <- mutants_per_line$"num_mutants" / mutants_per_line$"sloc"
cat("Min: ", min(ratio_mutants_per_line), "\n", sep="")
cat("Max: ", max(ratio_mutants_per_line), "\n", sep="")
cat("Mean: ", mean(ratio_mutants_per_line), "\n", sep="")
cat("Median: ", median(ratio_mutants_per_line), "\n", sep="")

#
# Ratio of mutants per mutated line = # of mutants per pid-bid / # unique mutated lines per pid-bid
#
cat("\n=== Ratio of mutants per mutated line ===\n", sep="")
mutants_per_mutated_line <- merge(mutants_per_line, mutated_lines, by=c("pid", "bid", "sloc"), all.x=TRUE)
ratio_mutants_per_mutated_line <- mutants_per_mutated_line$"num_mutants" / mutants_per_mutated_line$"num_unique_mutated_lines"
cat("Min: ", min(ratio_mutants_per_mutated_line), "\n", sep="")
cat("Max: ", max(ratio_mutants_per_mutated_line), "\n", sep="")
cat("Mean: ", mean(ratio_mutants_per_mutated_line), "\n", sep="")
cat("Median: ", median(ratio_mutants_per_mutated_line), "\n", sep="")

#
# Ratio of mutants per executable line = # of mutants per pid-bid / # unique executable lines per pid-bid
#
cat("\n=== Ratio of mutants per executable line ===\n", sep="")
# Count number of unique executable lines per pid-bid
executable_lines <- aggregate(line ~ pid + bid, data=executable_lines_df, FUN=function(x) length(unique(x)))
colnames(executable_lines)[which(names(executable_lines) == "line")] <- "num_unique_executable_lines"
# Merge data frames
mutants_per_executable_line <- merge(mutants_per_line, executable_lines, by=c("pid", "bid"), all.x=TRUE)
# Compute ratio
ratio_mutants_per_executable_line <- mutants_per_executable_line$"num_mutants" / mutants_per_executable_line$"num_unique_executable_lines"
cat("Min: ", min(ratio_mutants_per_executable_line), "\n", sep="")
cat("Max: ", max(ratio_mutants_per_executable_line), "\n", sep="")
cat("Mean: ", mean(ratio_mutants_per_executable_line), "\n", sep="")
cat("Median: ", median(ratio_mutants_per_executable_line), "\n", sep="")

