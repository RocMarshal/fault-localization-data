#!/usr/bin/env bash
# ------------------------------------------------------------------------------
# 
# This script prints to the stdout the status/progress of each incomplete
# killmap.
#
# Usage:
# ./get-status.sh
#   --data_dir <path>
#   [--help]
# 
# ------------------------------------------------------------------------------

SCRIPT_DIR=`pwd`

#
# Prints error message to the stdout and exit.
#
die() {
  echo "$@" >&2
  exit 1
}

# ------------------------------------------------------------------ Envs & Args

USAGE="Usage: ${BASH_SOURCE[0]} --data_dir <path> [help]"
if [ "$#" -ne "1" ] && [ "$#" -ne "2" ]; then
  die "$USAGE"
fi

DATA_DIR=""

while [[ "$1" = --* ]]; do
  OPTION=$1; shift
  case $OPTION in
    (--data_dir)
      DATA_DIR=$1;
      shift;;
    (--help)
      echo "$USAGE"
      exit 0;;
    (*)
      die "$USAGE";;
  esac
done

[ "$DATA_DIR" != "" ] || die "$USAGE"
[ -d "$DATA_DIR" ] || die "$DATA_DIR does not exist!"

# ------------------------------------------------------------------------- Main

echo "pid,bid,progress"

find "$DATA_DIR" -type f -name "log.txt" | while read -r log_file; do
  # /my/dir/killmaps/Time/2500264/log.txt
  pid=$(echo "$log_file" | grep -oP "killmaps/(.*)/(.*)/log.txt" | cut -f2 -d'/')
  bid=$(echo "$log_file" | grep -oP "killmaps/(.*)/(.*)/log.txt" | cut -f3 -d'/')

  if grep --text -q "^\[starting test " "$log_file"; then
    status=$(grep --text "^\[starting test " "$log_file" | tail -n1 | cut -f3 -d' ' | cut -f1 -d':')
    progress=$(echo "$status * 100" | bc -l)
  else
    progress="0"
  fi

  echo "$pid,$bid,$progress"
done

exit 0