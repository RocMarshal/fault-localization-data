#!/usr/bin/env bash
# ------------------------------------------------------------------------------
# 
# This script tests the conversion of a ranking of Java statements to a ranking
# of Java lines.
# 
# Usage:
# ./test_stmt-susps-to-line-susps.sh
# 
# ------------------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)

#
# Prints error message to the stdout and exit.
#
die() {
  echo "$@" >&2
  exit 1
}

# ------------------------------------------------------------------ Envs & Args

SUT="$SCRIPT_DIR/../../stmt-susps-to-line-susps"

TMP_DIR="/tmp/$USER-test_stmt-susps-to-line-susps-$$"
rm -rf "$TMP_DIR"
mkdir -p "$TMP_DIR" || die "Failed to create $TMP_DIR directory!"

# ------------------------------------------------------------------------- Util

_assert_files() {
  [ $# -eq 2 ] || die "Usage: ${FUNCNAME[0]} <actual> <expected>"

  local actual="$1"
  local expected="$2"

  [ -s "$actual" ] || die "$actual does not exist or it is empty"
  [ -s "$expected" ] || die "$expected does not exist or it is empty"

  sort "$actual" > "$actual.sorted"
  sort "$expected" > "$expected.sorted"

  cmp "$expected.sorted" "$actual.sorted" || die "'$actual' is not equal to '$expected'!"
  rm -f "$actual.sorted" "$expected.sorted"
}

# ------------------------------------------------------------------------- Main

test_ranking_without_repetitions_without_sub_lines() {
  local input_file="$TMP_DIR/input.${FUNCNAME[0]}.txt"
  echo "Statement,Suspiciousness" > "$input_file"
  echo "org.foo.Bar#1,0.4" >> "$input_file"
  echo "org.foo.Bar#2,0.3" >> "$input_file"
  echo "org.foo.Bar#3,0.2" >> "$input_file"

  local expected_output_file="$TMP_DIR/expected-output.${FUNCNAME[0]}.txt"
  echo "Line,Suspiciousness" > "$expected_output_file"
  echo "org/foo/Bar.java#2,0.3" >> "$expected_output_file"
  echo "org/foo/Bar.java#3,0.2" >> "$expected_output_file"
  echo "org/foo/Bar.java#1,0.4" >> "$expected_output_file"
  echo "org/foo/Bar.java#4,0.2" >> "$expected_output_file"

  local source_code_lines_file="$TMP_DIR/source_code_lines.${FUNCNAME[0]}.txt"
  echo "org/foo/Bar.java#3:org/foo/Bar.java#4" > "$source_code_lines_file"

  local outcome_file="$TMP_DIR/outcome.${FUNCNAME[0]}.txt"
  rm -f "$outcome_file"

  $SUT \
    --stmt-susps "$input_file" \
    --source-code-lines "$source_code_lines_file" \
    --output "$outcome_file" || die "test_ranking_without_repetitions_without_sub_lines has failed!"
  dos2unix -q "$outcome_file" || die

  _assert_files "$expected_output_file" "$outcome_file" || die
}

test_ranking_without_repetitions_with_sub_lines() {
  local input_file="$TMP_DIR/input.${FUNCNAME[0]}.txt"
  echo "Statement,Suspiciousness" > "$input_file"
  echo "org.foo.Bar#1,0.4" >> "$input_file"
  echo "org.foo.Bar#2,0.3" >> "$input_file"
  echo "org.foo.Bar#3,0.2" >> "$input_file"
  echo "org.foo.Bar#4,0.0" >> "$input_file"

  local expected_output_file="$TMP_DIR/expected-output.${FUNCNAME[0]}.txt"
  echo "Line,Suspiciousness" > "$expected_output_file"
  echo "org/foo/Bar.java#2,0.3" >> "$expected_output_file"
  echo "org/foo/Bar.java#3,0.2" >> "$expected_output_file"
  echo "org/foo/Bar.java#1,0.4" >> "$expected_output_file"
  echo "org/foo/Bar.java#4,0.0" >> "$expected_output_file"

  local source_code_lines_file="$TMP_DIR/source_code_lines.${FUNCNAME[0]}.txt"
  echo "org/foo/Bar.java#3:org/foo/Bar.java#4" > "$source_code_lines_file"

  local outcome_file="$TMP_DIR/outcome.${FUNCNAME[0]}.txt"
  rm -f "$outcome_file"

  $SUT \
    --stmt-susps "$input_file" \
    --source-code-lines "$source_code_lines_file" \
    --output "$outcome_file" || die "test_ranking_without_repetitions_without_sub_lines has failed!"
  dos2unix -q "$outcome_file" || die

  _assert_files "$expected_output_file" "$outcome_file" || die
}

test_ranking_with_repetitions_without_sub_lines() {
  local input_file="$TMP_DIR/input.${FUNCNAME[0]}.txt"
  echo "Statement,Suspiciousness" > "$input_file"
  echo "org.foo.Bar#1,0.4" >> "$input_file"
  echo "org.foo.Bar#2,0.3" >> "$input_file"
  echo "org.foo.Bar#2,0.0" >> "$input_file"
  echo "org.foo.Bar#3,0.2" >> "$input_file"

  local expected_output_file="$TMP_DIR/expected-output.${FUNCNAME[0]}.txt"
  echo "Line,Suspiciousness" > "$expected_output_file"
  echo "org/foo/Bar.java#2,0.3" >> "$expected_output_file"
  echo "org/foo/Bar.java#3,0.2" >> "$expected_output_file"
  echo "org/foo/Bar.java#1,0.4" >> "$expected_output_file"
  echo "org/foo/Bar.java#4,0.2" >> "$expected_output_file"

  local source_code_lines_file="$TMP_DIR/source_code_lines.${FUNCNAME[0]}.txt"
  echo "org/foo/Bar.java#3:org/foo/Bar.java#4" > "$source_code_lines_file"

  local outcome_file="$TMP_DIR/outcome.${FUNCNAME[0]}.txt"
  rm -f "$outcome_file"

  $SUT \
    --stmt-susps "$input_file" \
    --source-code-lines "$source_code_lines_file" \
    --output "$outcome_file" || die "test_ranking_without_repetitions_without_sub_lines has failed!"
  dos2unix -q "$outcome_file" || die

  _assert_files "$expected_output_file" "$outcome_file" || die
}

test_ranking_with_repetitions_with_sub_lines() {
  local input_file="$TMP_DIR/input.${FUNCNAME[0]}.txt"
  echo "Statement,Suspiciousness" > "$input_file"
  echo "org.foo.Bar#1,0.4" >> "$input_file"
  echo "org.foo.Bar#2,0.3" >> "$input_file"
  echo "org.foo.Bar#2,0.0" >> "$input_file"
  echo "org.foo.Bar#3,0.2" >> "$input_file"
  echo "org.foo.Bar#4,0.0" >> "$input_file"

  local expected_output_file="$TMP_DIR/expected-output.${FUNCNAME[0]}.txt"
  echo "Line,Suspiciousness" > "$expected_output_file"
  echo "org/foo/Bar.java#2,0.3" >> "$expected_output_file"
  echo "org/foo/Bar.java#3,0.2" >> "$expected_output_file"
  echo "org/foo/Bar.java#1,0.4" >> "$expected_output_file"
  echo "org/foo/Bar.java#4,0.0" >> "$expected_output_file"

  local source_code_lines_file="$TMP_DIR/source_code_lines.${FUNCNAME[0]}.txt"
  echo "org/foo/Bar.java#3:org/foo/Bar.java#4" > "$source_code_lines_file"

  local outcome_file="$TMP_DIR/outcome.${FUNCNAME[0]}.txt"
  rm -f "$outcome_file"

  $SUT \
    --stmt-susps "$input_file" \
    --source-code-lines "$source_code_lines_file" \
    --output "$outcome_file" || die "test_ranking_without_repetitions_without_sub_lines has failed!"
  dos2unix -q "$outcome_file" || die

  _assert_files "$expected_output_file" "$outcome_file" || die
}

test_ranking_without_repetitions_without_sub_lines || die
test_ranking_without_repetitions_with_sub_lines || die
test_ranking_with_repetitions_without_sub_lines || die
test_ranking_with_repetitions_with_sub_lines || die

rm -rf "$TMP_DIR"
exit 0
