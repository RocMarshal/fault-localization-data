#!/usr/bin/env bash
#
# ------------------------------------------------------------------------------
# This script collects all executable lines per project/bug. The output file
# follows the following format:
#
#  pid,bid,line
#
# Where <pid> is the project name, <bid> the fault id, and <line> the executable
# line (class:line_number)
#
# For example:
#  pid,bid,line
#  ...
#  Chart,24,org.jfree.chart.renderer.GrayPaintScale:126
#  Chart,24,org.jfree.chart.renderer.GrayPaintScale:126
#  Chart,25,org.jfree.chart.renderer.category.StatisticalBarRenderer:259
#
# Usage:
# ./get_executable_lines.sh <output_file>
#
# Requirements:
# - The environment variable D4J_HOME needs to be set and must point to the
#   Defects4J installation.
# - GZOLTAR_FILES_DIR   Needs to be set and must point to GZoltar tar.gz files.
#
# ------------------------------------------------------------------------------

HERE=$(cd `dirname ${BASH_SOURCE[0]}` && pwd)

#
# Print error message and exit
#
die() {
  echo "$@" >&2
  exit 1
}

# ------------------------------------------------------------------ Envs & Args

# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "[ERROR] D4J_HOME is not set!"
[ -d "$D4J_HOME" ] || die "$D4J_HOME does not exist!"

# Check whether GZOLTAR_FILES_DIR is set
[ "$GZOLTAR_FILES_DIR" != "" ] || die "GZOLTAR_FILES_DIR is not set!"
[ -d "$GZOLTAR_FILES_DIR" ] || die "$GZOLTAR_FILES_DIR does not exist!"

BLACKLIST_FILE="$HERE/../data/blacklist.csv"
[ -s "$BLACKLIST_FILE" ] || die "$BLACKLIST_FILE does not exist or it is empty!"

USAGE="Usage: ${BASH_SOURCE[0]} <output_file>"
[ "$#" -eq "1" ] || die "$USAGE"

OUTPUT_FILE="$1"
echo "pid,bid,line" > "$OUTPUT_FILE" || die "Cannot write to $OUTPUT_FILE!"

# ------------------------------------------------------------------------- Main

TMP_DIR="/tmp/$$-get_executable_lines"
mkdir -p "$TMP_DIR"

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"
  [ -d "$dir_project" ] || die "$dir_project does not exist!"

  for bid in $(cut -f1 -d',' "$dir_project/commit-db"); do
    if [ "$bid" -gt "1000" ]; then
      # skip artificial faults
      break
    fi

    echo "$pid-$bid"

    #
    # Unpack GZoltar file
    #

    gzoltar_file="$GZOLTAR_FILES_DIR/$pid/$bid/gzoltar-files.tar.gz"
    if [ ! -s "$gzoltar_file" ]; then
      rm -rf "$TMP_DIR"
      die "[ERROR] $gzoltar_file does not exist or it is empty!"
    fi

    gz_tmp_dir="$TMP_DIR/$pid"
    mkdir -p "$gz_tmp_dir"

    tar xzf "$gzoltar_file" -C "$gz_tmp_dir"
    if [ $? -ne 0 ]; then
      rm -rf "$TMP_DIR"
      die "[ERROR] Extraction of '$gzoltar_file' has failed!"
    fi

    matrix_file="$gz_tmp_dir/gzoltars/$pid/$bid/matrix"
    if [ ! -s "$matrix_file" ]; then
      rm -rf "$TMP_DIR"
      die "[ERROR] $matrix_file does not exist or it is empty!"
    fi
    spectra_file="$gz_tmp_dir/gzoltars/$pid/$bid/spectra"
    if [ ! -s "$spectra_file" ]; then
      rm -rf "$TMP_DIR"
      die "[ERROR] $spectra_file does not exist or it is empty!"
    fi

    num_tests=$(wc -l "$matrix_file" | cut -f1 -d' ')
    test_id=1

    echo "Get coverage per test case ..."
    cov_file="$gz_tmp_dir/cov.txt"
    >"$cov_file"
    while read -r test_coverage; do
      echo "$test_id out of $num_tests"
      echo "$test_coverage" | awk '{for (i = 1; i <= NF; ++i) if ($i == 1) print i}' >> "$cov_file"
      test_id=$((test_id+1))
    done < <(cat "$matrix_file")

    # Get unique list of lines and prepend information about the pid-bid
    echo "Get unique list of lines ..."
    while read -r line_id; do
      sed "${line_id}q;d" "$spectra_file" | sed "s/^/$pid,$bid,/" >> "$OUTPUT_FILE"
    done < <(sort -u "$cov_file")
    (head -n1 "$OUTPUT_FILE" && (tail -n +2 "$OUTPUT_FILE" | sort -u)) > "$OUTPUT_FILE.tmp"
    mv "$OUTPUT_FILE.tmp" "$OUTPUT_FILE" || die "[ERROR] Failed to replace $OUTPUT_FILE with $OUTPUT_FILE.tmp!"

    rm -rf "$gz_tmp_dir"
  done
done

rm -rf "$TMP_DIR" # Clean up temporary data

#
# Sanity checks
#

num_entries=$(wc -l "$OUTPUT_FILE" | cut -f1 -d' ')
[ "$num_entries" -ge "2" ] || die "[ERROR] $OUTPUT_FILE should has at least two rows but instead it has $num_entries!"

if grep -q ",$" "$OUTPUT_FILE"; then
  die "[ERROR] $OUTPUT_FILE is not well formatted!"
fi

echo "DONE!"
exit 0
