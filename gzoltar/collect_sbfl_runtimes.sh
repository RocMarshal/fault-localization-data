#!/usr/bin/env bash
#
# ------------------------------------------------------------------------------
# This script collects the time to run GZoltar on all D4J's faults. The runtime
# (in seconds) of each fault is written to provided file.
#
# Usage:
# ./collect_sbfl_runtimes.sh
#   --input_dir <path>
#   --output_file <path>
#   [--help]
#
# Environment variables:
# - D4J_HOME   Needs to be set and must point to the Defects4J installation.
#
# ------------------------------------------------------------------------------

SCRIPT_DIR=`pwd`
source "$SCRIPT_DIR/utils.sh" || exit 1

# ------------------------------------------------------------------ Envs & Args

# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "D4J_HOME is not set!"
[ -d "$D4J_HOME" ] || die "$D4J_HOME does not exist!"

# Check whether BLACKLIST_FILE exists
BLACKLIST_FILE="$SCRIPT_DIR/../data/blacklist.csv"
[ -s "$BLACKLIST_FILE" ] || die "$BLACKLIST_FILE file does not exist or it is empty!"

USAGE="Usage: ${BASH_SOURCE[0]} --input_dir <path> --output_file <path> [help]"
[ "$#" -ne "1" ] || [ "$#" -eq "4" ] || die "$USAGE"

INPUT_DIR=""
OUTPUT_FILE=""

while [[ "$1" = --* ]]; do
  OPTION=$1; shift
  case $OPTION in
    (--input_dir)
      INPUT_DIR=$1;
      shift;;
    (--output_file)
      OUTPUT_FILE=$1;
      shift;;
    (--help)
      echo "$USAGE"
      exit 0;;
    (*)
      die "$USAGE";;
  esac
done

[ "$INPUT_DIR" != "" ] || die "$USAGE"
[ "$OUTPUT_FILE" != "" ] || die "$USAGE"

[ "$INPUT_DIR" != "" ] || die "$USAGE"
[ -d "$INPUT_DIR" ] || die "$INPUT_DIR does not exist!"

echo "project,fault,runtime" > "$OUTPUT_FILE" || die "Cannot write to $OUTPUT_FILE!"

# --------------------------------------------------------------- Util functions

#
# Convert `date` to epoch
#
# (based on https://stackoverflow.com/a/7241238/998816)
#
_convert_to_epoch() {
  local USAGE="Usage: ${FUNCNAME[0]} <output of \`date\`> <date format>"
  if [ "$#" != 2 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  python - "$1" "$2" << END
import sys
import time
print(int(time.mktime(time.strptime(sys.argv[1], sys.argv[2]))))
END

  return "$?"
}

#
# Subtract two dates, i.e., <date 1> - <date 2>
#
_subtract_dates() {
  local USAGE="Usage: ${FUNCNAME[0]} <date 1> <date 2> <date format>"
  if [ "$#" != 3 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local a=$(_convert_to_epoch "$1" "$3")
  local b=$(_convert_to_epoch "$2" "$3")

  echo "$a - $b" | bc || return 1

  return 0
}

#
# Convert a string %H:%M:%S into seconds
#
_convert_to_seconds() {
  local USAGE="Usage: ${FUNCNAME[0]} <\`date\`, format %H:%M:%S>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local h=$(echo "$1" | cut -f1 -d':')
  local m=$(echo "$1" | cut -f2 -d':')
  local s=$(echo "$1" | cut -f3 -d':')

  echo "$h"*3600 + "$m"*60 + "$s" | bc || return 1

  return 0
}

# ------------------------------------------------------------------------- Main

DATE_DEFAULT_FORMAT="%a,%b,%d,%H:%M:%S,%Z,%Y"
HOUR_MIN_SEC_FORMAT="%H:%M:%S"

TMP_DIR="/tmp/$USER-collect_sbfl_runtimes-$$"
rm -rf "$TMP_DIR"
mkdir "$TMP_DIR"

for pid in Chart Closure Lang Math Mockito Time; do

  for bid in $(cut -f1 -d',' "$D4J_HOME/framework/projects/$pid/commit-db"); do

    if grep -q "^$pid,.*,$bid," "$BLACKLIST_FILE"; then
      continue;
    fi

    if [ "$bid" -gt "1000" ]; then
      MUTANTS_IN_SCOPE="$D4J_HOME/framework/projects/$pid/mutants_in_scope.csv"
      if ! grep -q "^$pid,.*,$bid$" "$MUTANTS_IN_SCOPE"; then
        continue # not in scope
      fi
    fi

    gzoltar_file="$INPUT_DIR/$pid/$bid/gzoltar-files.tar.gz"
    [ -s "$gzoltar_file" ] || continue

    echo "$pid-$bid"

    # Extract data
    tar -zxf "$gzoltar_file" -C "$TMP_DIR/" || die "Extraction of '$gzoltar_file' to '$TMP_DIR/' has failed!"

    # e.g., [INFO] Start: Tue Feb 26 15:31:00 PST 2019
    start=$(grep --text "^\[INFO\] Start: " "$TMP_DIR/gzoltars/$pid/$bid/log.txt" | awk -F' ' '{printf "%s,%s,%s,%s,%s,%s\n", $3, $4, $5, $6, $7, $8;}')
    # e.g., [INFO] End: Tue Feb 26 15:32:55 PST 2019
    end=$(grep --text "^\[INFO\] End: " "$TMP_DIR/gzoltars/$pid/$bid/log.txt" | awk -F' ' '{printf "%s,%s,%s,%s,%s,%s\n", $3, $4, $5, $6, $7, $8;}')
    runtime=$(_subtract_dates "$end" "$start" "$DATE_DEFAULT_FORMAT")

    [ "$runtime" != "" ] || die "failed to compute runtime of $pid-$bid"

    echo "$pid,$bid,$runtime" >> "$OUTPUT_FILE"

    rm -rf "$TMP_DIR/gzoltars/$pid/$bid" # clean up
  done
done

# Sanity check
if grep -q ",$" "$OUTPUT_FILE"; then
  die "$OUTPUT_FILE is not well formatted!"
fi

rm -rf "$TMP_DIR" # clean up

echo "DONE!"
exit 0
